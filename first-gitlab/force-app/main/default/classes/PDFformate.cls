public class PDFformate {
    @Future(callout=true)
    public static void sendMail(List<ID> listid){
        //ID Id = ApexPages.currentPage().getParameters().get('id');
        List<Candidate_Sanam__c> crecord = [SELECT id,Email__c FROM Candidate_Sanam__c WHERE id IN: listid];
        //system.debug('candidate record');
        //system.debug(crecord);
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        for(Candidate_Sanam__c candidate : crecord) {
            system.debug(candidate);
            //getting data from VF page
            PageReference pdf = Page.CandidateDetailPDF;
            pdf.getParameters().put('id',candidate.id);
            pdf.setRedirect(true);
            
            Blob b = pdf.getContentAsPDF();
            
            //creating the File Attachment for Mail
            List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setFileName('CandidateDetails.pdf');
            efa.setBody(b);
            fileAttachments.add(efa);
            
            //Creating email structure
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();    
            List<String> sendTo = new List<String>();
            sendTo.add(candidate.Email__c);
            mail.setToAddresses(sendTo);
            mail.setSubject('Job Offer');  
            mail.setPlainTextBody( 'Congrats You are Hired' );
            mail.setFileAttachments(fileAttachments);
            mails.add(mail);
            
        }
        system.debug('mail sent');
        Messaging.sendEmail(mails);    //sending mails
    }
}