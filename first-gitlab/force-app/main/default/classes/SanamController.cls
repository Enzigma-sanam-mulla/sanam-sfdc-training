public class SanamController {

public Candidate_Sanam__c A {get; private set;}

    public SanamController (){
       Id id =ApexPages.currentPage().getParameters().get('id');
      A= (id==null)?new Candidate_Sanam__c ():[select Name,First_Name__c,Last_Name__c,Email__c,State__c,Status__c,Job_Sanam__c from Candidate_Sanam__c where Id=:id];
   }
   
    public PageReference save(){
       try{
           upsert(A);
       }
       catch(System.DMLException e)
       {
           ApexPages.addMessages(e);
           return null;
       }
       PageReference redirectSuccess = new ApexPages.StandardController(A).View();
       return redirectSuccess;
   }

}