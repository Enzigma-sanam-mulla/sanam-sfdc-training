public class candidateHandlerClass {
//--------------//Task1--------------
    public static void candidateMissedEnterExpectedSalaryError (List<Candidate_Sanam__c> candidateObject){          
        for(Candidate_Sanam__c cadidateData : candidateObject){ 
            if(cadidateData.Expected_Salary__c == null){
                cadidateData.addError('Expected Salary field is missing');
            }                                      	
        }
    }  
    
    public static void candidateApplyingJobStatusNotActiveError (List<Candidate_Sanam__c> candidateObject){
        
        Map<Id,Job_Sanam__c> JActive = new Map<Id,Job_Sanam__c>([SELECT id,Active__c 
                                                                FROM Job_Sanam__c 
                                                                where Active__c=false]);
        
        for(Candidate_Sanam__c Cdata:candidateObject){
            if(JActive.get(Cdata.Job_Sanam__c)!=null){
                Cdata.addError('This Job is not active. You can not apply for this job. ');
            }
        }       
    }

    
//---------------------//Task2---------------------------    
 public static void systemGeneratedCreatedApplicationDateAdd(List<Candidate_Sanam__c> Cobj){
        for(Candidate_Sanam__c Cdata : Cobj){    
           Cdata.Application_Date__c = system.today();
            
        }
 }
  /*
    public static void systemGeneratedCreatedApplicationDateAdd(List<Candidate_Sanam__c> Cobj){
        Map<Id,Candidate_Sanam__c> MCobj= new Map<Id,Candidate_Sanam__c>([select ID from Candidate_Sanam__c where Email__c!=null]);//selective Q
        for(Candidate_Sanam__c Cdata:Cobj){    
           	//List<Candidate_Sanam__c> M = new List<Candidate_Sanam__c>([select CreatedDate from Candidate_Sanam__c where ID!=:Id]);
            Cdata.Application_Date__c=M;
            

			//Cdata.Application_Date__c=system.today();          
            //SELECT CreatedDate FROM Account WHERE Id='0012f00000cNzhB'; a075g000001nsPl
        }
    }
*/
}