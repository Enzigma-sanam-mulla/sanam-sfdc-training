public class PBBuilder {
    @InvocableMethod
    public static void populateAccountAddress(List<Id> contactId)
    {   
        //System.debug('contactId' + contactId);
        List<Contact> lstContact = new List<Contact>();
        List<String> lstmail = new List<String>();
        Set<Id> accIds = new Set<Id>();
        List<Account> lstAccount = new List<Account>();
        for(Contact obj : [select AccountId from Contact where id in : contactId]){
            accIds.add(obj.AccountId);
        }
        System.debug('accId'+accIds);        
        lstAccount = [select Name,BillingStreet,BillingCity,BillingState,BillingPostalCode,BillingCountry,(Select Id,Email from Contacts where id =:contactId ) from Account where id in : accIds];
        System.debug('lstAccount' + lstAccount);              
        for(Account objAccount : lstAccount){                
            //System.debug('objAccount' + objAccount);
            for(Contact objContact:objAccount.Contacts){
                lstmail.add(objContact.Email);
                objContact.OtherStreet = objAccount.BillingStreet;
                objContact.OtherCity = objAccount.BillingCity;
                objContact.OtherState = objAccount.BillingState;
                objContact.OtherPostalCode = objAccount.BillingPostalCode;
                objContact.OtherCountry= objAccount.BillingCountry;
                update objContact;
            }
        }
        
        
        System.debug('lstmail' +lstmail);
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.toAddresses = lstmail ;
        message.optOutPolicy = 'FILTER';
        message.subject = 'you are hired';
        message.plainTextBody = 'you are hired... congratulations.';
        Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
            Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        
        if (results[0].success) 
        {
            System.debug('The email was sent successfully.');
        } else 
        {
            System.debug('The email failed to send: ' + results[0].errors[0].message);
        }       
    }   
}