public class JobRegister {

    public Job_Sanam__c job{
        get;
        private set;
    }
    
    public JobRegister(){
       Id id =ApexPages.currentPage().getParameters().get('id');
      job= (id==null)?new Job_Sanam__c():[select Name,Number_of_Positions__c,Required_Skills__c,Qualification_Required__c,Certification_Required__c from Job_Sanam__c where Id=:id];
   }

    public PageReference save() {
        try{
            upsert(job);
        }
        catch(System.DMLException e){
            ApexPages.addMessages(e);
            return null;
        }
         PageReference redirectSuccess = new ApexPages.StandardController(job).View();
       return redirectSuccess;
    }
}