public class MyControllerExtension {
    public final Account acct;
    public MyControllerExtension(ApexPages.StandardController stdController) {
        this.acct=(Account)stdController.getRecord();
    }
    public String Greeting(){
        return 'hello '+acct.name + '('+acct.id+ ')';
    }
}