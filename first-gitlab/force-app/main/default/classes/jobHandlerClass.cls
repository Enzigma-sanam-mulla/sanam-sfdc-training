public class jobHandlerClass {
    //---------------------//Task3---------------------------      
    public static void jobStatusActiveNotDeletedError(List<Job_Sanam__c> lstJobSanam){
        //List<Job_Sanam__c> lstJobFilterData = new List<Job_Sanam__c>([SELECT id,Active__c
        //                                                             FROM Job_Sanam__c
        //                                                            WHERE id IN: lstJobSanam AND Active__c=true]);
        
        for(Job_Sanam__c jobData : lstJobSanam){          
            if(jobData.Active__c ){
                jobData.addError('This Job is active and can not be deleted...!');                
            }
        }        
    }
    
    //---------------------//Task4---------------------------      
    public static void numberCandidatesHiredEqualsNumberPositionsDeactivatedCandidatesApplyJob(List<Job_Sanam__c> jObj){
        for(Job_Sanam__c data : jObj){   
            if(data.Number_of_Positions__c <= data.Hired_Applicants__c){
                data.Active__c=false;
            }
        }
    }
    
    //---------------------//Task5-------------------------- 
    public static void positionsJobEqualNumberHiredCandidatesEmail(List<Job_Sanam__c> newList){
        Set<Id> contactId = new set<Id>();
        List<Contact> contactList = new List<Contact>();
        Integer i = 0;    
        boolean flag = false;
        for(i=0;i<=1;i++){
            for(Job_Sanam__c job : newList){
                contactId.add(job.Manager__c);
                contactList = [select Email from Contact where id =:contactId];
            }
        }
        
        for(Job_Sanam__c job : newList)
        {
            if(job.Number_of_Positions__c == job.Hired_Applicants__c)
            {
                if(job.Manager__c != null){
                    List<Contact> contactEmail = contactList;
                    for(Contact con : contactEmail){
                        if(con.Email != null){
                            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                            message.toAddresses = new String[] { con.Email };
                            message.optOutPolicy = 'FILTER';
                            message.subject = 'vacancy Related';
                            message.plainTextBody = '“All required candidate has been hired for this job on '+job.LastModifiedDate;
                            Messaging.SingleEmailMessage[] messages =
                            new List<Messaging.SingleEmailMessage> {message};
                            Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
                        }
                    }
                }
            }
        }
    }
    //---------------------//Task6--------------------------      
    public static void autoActivatedNoPositionsGreaterThanHire(List<Job_Sanam__c> Jobj){
        for(Job_Sanam__c data:Jobj){
            if(data.Number_of_Positions__c > data.Hired_Applicants__c ){
                data.Active__c=true;
            }
        }       
    }
   
}