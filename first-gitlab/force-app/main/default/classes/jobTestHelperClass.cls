@isTest
public class jobTestHelperClass {   
    @testSetup static void setup() {
        // Create common test manager
        List<Contact> testContacts = new List<Contact>();
        List<Job_Sanam__c> testJob = new List<Job_Sanam__c>();
        List<Candidate_Sanam__c> testCandidate = new List<Candidate_Sanam__c>();
        
        //Contact -> Manager
        for(Integer i=1;i<6;i++) {
            testContacts.add(new Contact(LastName = 'ContactManager'+i));
        }  
        Insert testContacts;
        //Job
        for(Integer i=1;i<6;i++){
            testJob.add(new Job_Sanam__c(Number_of_Positions__c = i,
                                         Active__c = true,
                                         Salary_Offered__c = 100,
                                         Manager__c = testContacts[0].id,
                                         Job_Type__c = 'Manager',
                                         Required_Skills__c = 'People Skills',
                                         Qualification_Required__c = 'Bcom',
                                         Certification_Required__c = 'CMP (Certified Project Manager)'
                                         
                                        ));
        }
        Insert testJob;
        //Candidate
        for(Integer i=1;i<6;i++){
            testCandidate.add(new Candidate_Sanam__c(First_Name__c = 'Rohan',
                                                     Last_Name__c = 'Rao',
                                                     Email__c = 'samad'+i+'@gmail.com',
                                                     Country__c = 'india',
                                                     State__c = 'Gujrat',
                                                     Expected_Salary__c = 4000+i,
                                                     Status__c = 'Applied', 
                                                     Job_Sanam__c = testJob[0].Id
                                                    ));
        }
        Insert testCandidate;
    }
    
    
    
    
    //Test Setup
    @isTest static void checkJobActivateORDeActivate(){                   
        //Test
        List<Candidate_Sanam__c> cansanam = new List<Candidate_Sanam__c>();
        cansanam = [select First_Name__c,Last_Name__c,Email__c,Country__c,State__c,Expected_Salary__c,Status__c from Candidate_Sanam__c];
        Test.startTest();
        Database.SaveResult[] result = Database.insert(cansanam, false);
        System.debug(result);
        Test.stopTest();
        for (Database.SaveResult sr : result) {
            System.assertEquals(false, sr.isSuccess());
            //assertEquals(expected[i],actual[i])
            System.assertEquals('Expected Salary field is missing',sr.getErrors()[0].getMessage());
        }                  
    }
    
    public static testMethod void appDateUnitTest()
    {
        List<Candidate_Sanam__c> cnList=new List<Candidate_Sanam__c>();
        Candidate_Sanam__c cnObj=new Candidate_Sanam__c();
        cnObj.Application_Date__c=System.today();
        
        cnList.add(cnObj);
        Test.startTest();    
        Database.SaveResult[] sr = Database.insert(cnList,false);
        Test.stopTest();
    }
    
    //for job
    public static testMethod void preventDeleteUnitTest()					
    {        
        Test.startTest();
        try{
            List<Job_Sanam__c> jobList=[select Active__c from Job_Sanam__c where Active__c = true];
            Delete jobList;
        }
        catch(Exception e)
        {
            String message = e.getMessage();
            System.assert(message.contains('Delete failed.'));
            system.assert(message.contains('This Job is active and can not be deleted'));
        }
        Test.stopTest();
    }
    
    
    //for job
    public static testMethod void deleteUnitTest()							
    {
        Job_Sanam__c jobObj = new Job_Sanam__c();
        jobObj.Name='Test_Job';
        jobObj.Active__c = false;
        
        Candidate_Sanam__c cnObj=new Candidate_Sanam__c();
        List<Candidate_Sanam__c> cnList =new List<Candidate_Sanam__c>();
        cnObj.name = 'ajay';
        cnObj.Job_Sanam__c = jobObj.id;
        cnList.add(cnObj);
        
        Test.startTest();
        Database.SaveResult[] sr = Database.insert(cnList,false);
        Test.stopTest();
        try{
            System.assertEquals(1,sr.size());
        }
        catch(DmlException e)
        {
            String message = e.getMessage();          
            system.assert(message.contains('This Job is not active. You can not apply for this job'));
        }
    }
}