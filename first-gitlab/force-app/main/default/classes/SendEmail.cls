public class SendEmail {
    @future(callout=true)
    public static void SendToCandidate(Set<Id> CandidateIds){
        Candidate_Sanam__c CandidateInfo = new Candidate_Sanam__c();
        CandidateInfo = [SELECT Id,Email__c
                         FROM Candidate_Sanam__c 
                         WHERE Id IN : CandidateIds];
        System.debug('CandidateInfo'+CandidateInfo);
        
        /* Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
message.toAddresses = new String[] { CandidateInfo.Email__c };
message.optOutPolicy = 'FILTER';
message.subject = 'you are hired';
message.plainTextBody = 'you are hired... congratulations.';
Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);

if (results[0].success) 
{
System.debug('The email was sent successfully.');
} else 
{
System.debug('The email failed to send: ' + results[0].errors[0].message);
}
*/
        
        Messaging.SingleEmailMessage semail = new Messaging.SingleEmailMessage();
        Messaging.EmailFileAttachment attach1 = new Messaging.EmailFileAttachment();
        PageReference pref = page.CandidateDetailPDF;
        pref.getParameters().put('id',(String)CandidateInfo.id);
        pref.setRedirect(true);
        Blob b = pref.getContent();
        	System.debug('b'+b);
        attach1.setFileName('CandidateDetailPDF.pdf');
        attach1.setBody(b);
        semail.setSubject('hired');
        String[] sendTo = new String[]{CandidateInfo.Email__c};
            System.debug('sendTo'+sendTo);
        semail.setToAddresses(sendTo);
        semail.setPlainTextBody('you are hired... congratulations');
        semail.setFileAttachments(new Messaging.EmailFileAttachment[]{attach1});
        Messaging.sendEmail(new Messaging.SingleEmailMessage[]{semail});
        System.debug('semail' +semail);
        
        Attachment attach = new Attachment();        
        attach.parentId = (String)CandidateInfo.id;
        attach.Name = 'CandidateDetailPDF.pdf';
        	System.debug('attach.Name '+attach.Name);
        attach.body = b;
        	System.debug('b '+b);
        System.debug('attach.body '+attach1.body);
        insert attach;
		System.debug('attach' +attach);        
    }  
}