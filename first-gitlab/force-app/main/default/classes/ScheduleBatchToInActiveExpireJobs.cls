global class ScheduleBatchToInActiveExpireJobs implements schedulable{
    global void execute(schedulableContext JobObj){
        BatchToInActiveExpireJobs BatchObj = new BatchToInActiveExpireJobs();
        Database.executeBatch(BatchObj, 200);
    }
}