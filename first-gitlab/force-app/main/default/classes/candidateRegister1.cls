public class candidateRegister1 {

    public Candidate_Sanam__c candidate{
        get;
        private set;
    }
    
    public candidateRegister1(){
       Id id =ApexPages.currentPage().getParameters().get('id');
      candidate= (id==null)?new Candidate_Sanam__c():[select Name,First_Name__c,Last_Name__c,Email__c,Status__c from Candidate_Sanam__c where Id=:id];
   }

    public PageReference save() {
        try{
            upsert(candidate);
        }
        catch(System.DMLException e){
            ApexPages.addMessages(e);
            return null;
        }
         PageReference redirectSuccess = new ApexPages.StandardController(candidate).View();
       return redirectSuccess;
    }
}