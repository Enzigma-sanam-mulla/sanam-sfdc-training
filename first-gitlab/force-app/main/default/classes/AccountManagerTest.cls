@IsTest
private class AccountManagerTest{
    @isTest static void testAccountManager(){
        Id recordId = getTestAccountId();
        system.debug('recordId' +recordId);
        // Set up a test request
        RestRequest request = new RestRequest();
        request.requestUri = 'https://ap5.salesforce.com/services/apexrest/Accounts/'+ recordId +'/contacts';
        request.httpMethod = 'GET';
        RestContext.request = request;
        
        // Call the method to test
        Account  acc = AccountManager.getAccount();
        
        // Verify results
        System.assert(acc != null);
    }
    
    private static Id getTestAccountId(){
        Account acc = new Account(Name = 'TestAcc2');
        Insert acc;
        system.debug('acc******' +acc);
         
        Contact con = new Contact(LastName = 'dumydata', AccountId = acc.Id);
        Insert con;
        system.debug('con*****' +con);
        
        return acc.Id;
    }
}