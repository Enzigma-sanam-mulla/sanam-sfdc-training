trigger CandidateTrigger on Candidate_Sanam__c (before insert,before delete) { 
    
    //TriggerSetting__c cObj = TriggerSetting__c.getInstance('CandidateTrigger');
    //System.debug('cObj' + cObj);
    //System.debug('cObj' + cObj.IsActive__c);   
    //if(cObj.IsActive__c==true){   
    
    //Task1
    if(Trigger.isBefore){
        if(Trigger.isInsert){             
            CandidateTriggerHandler.candidateMissedEnterExpectedSalaryError(Trigger.new);
            CandidateTriggerHandler.candidateApplyingJobStatusNotActiveError(Trigger.new);
        }
    }   
    
    //Task2 
    if(Trigger.isAfter){
        if(Trigger.isUpdate){
            CandidateTriggerHandler.createdApplicationDateAdd(Trigger.new);           
        }
    }   
    //}
}